using System;
namespace BreakItV1 {
	public class Ball {
		private int _xinit;
		private int _yinit;
        
        private int _height;

		public int XPosition { get; set; }
        public int YPosition { get; set; }

        public int XPositionOld { get; private set; }
        public int YPositionOld { get; private set; }

        public Direction hdirection { get; set; }
        public Direction vdirection { get; set; }
        public int Angle { get; set; }

        public Ball(int xposition, int yposition, int height)
        {
            _height = height;
            XPosition = _xinit = xposition;
			YPosition = _yinit = yposition;
            Angle = 1;
            vdirection = Direction.UP;
            hdirection = Direction.RIGHT;
        }
       
		public void Move() {
            XPositionOld = XPosition;
            YPositionOld = YPosition;

            if ((YPosition > 0 && YPosition < _height-1) && vdirection == Direction.UP)
            {
                YPosition--;

                if (YPosition % Angle == 0 && hdirection == Direction.RIGHT)
                    XPosition++;
                if (YPosition % Angle == 0 && hdirection == Direction.LEFT)
                    XPosition--;
            }

            if ((YPosition > 0 && YPosition < _height-1) && vdirection == Direction.DOWN)
            {
                YPosition++;

                if (YPosition % Angle == 0 && hdirection == Direction.RIGHT)
                   XPosition++;
                if (YPosition % Angle == 0 && hdirection == Direction.LEFT)
                   XPosition--;
            }
        }
		public void Clear() {
			throw new System.Exception("Not implemented");
		}
		
		public void Reset() {
            XPosition = _xinit;
            YPosition = _yinit;
            vdirection = Direction.UP;
		}
	}
}
