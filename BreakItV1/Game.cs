using System;

namespace BreakItV1
{
	public class Game
    {
		private Level _level;
		private int _score = 0;

        public const int GAMEWIDTH = 46; //51 56 61 66 71
        public static readonly int GAMEHEIGHT = 18; 
        public static readonly int BRICKROWS = 2;
        public static int PADDLESIZE = 7; //should be odd
        public static readonly int LIVES = 3;

        public void Play()
        {
            Console.CursorVisible = false;
            Console.Clear();
            ConsoleKeyInfo key = new ConsoleKeyInfo();
            Console.ForegroundColor = ConsoleColor.Black;

            bool exit = false;
            _level = new Level(LIVES,PADDLESIZE,GAMEWIDTH,GAMEHEIGHT, BRICKROWS);

            //game control loop
            while (!exit)
            {
                if (Console.KeyAvailable)
                {
                    key = Console.ReadKey(true);
                }

                if (key.Key == ConsoleKey.RightArrow)
                    _level.paddledirection = Direction.RIGHT;
                if (key.Key == ConsoleKey.LeftArrow)
                    _level.paddledirection = Direction.LEFT;
                if (key.Key == ConsoleKey.Escape)
                {
                    exit = true;
                }

                int exitcode = _level.Update();

                if (exitcode == 1)
                {
                    exit = true;
                    _score += _level.Score;
                    Console.WriteLine("Game finished, total Score: "+_score);
                }

                if (exitcode == 2)
                {
                    Console.WriteLine("Level finished");
                    _score += _level.Score;
                    Game.PADDLESIZE -= 2;
                    if (Game.PADDLESIZE < 3)
                        Game.PADDLESIZE = 3;
                    _level = new Level(LIVES, PADDLESIZE, GAMEWIDTH, GAMEHEIGHT, BRICKROWS);
                }

                System.Threading.Thread.Sleep(30);
            }
        }

		public static void Main(string[] args)
        {
			Game game = new Game();
			game.Play();
		}

	}

}
