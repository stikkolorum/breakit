using System;

namespace BreakItV1 {
	public class Row {
		
        private char _brickcharacter = '█';
        private char _spacecharacter = ' ';

        public char[] Bricks { get; set; }
        public int Brickvalue { get; private set; }
        public int Color { get; private set; }

        public Row(int width, int color)
        {
            Bricks = new char[width-2];
            Brickvalue = 20;
            Color = color;
                       
            int count = 0;
            for (int i=0; i < width-2; i++)
            {
                if (count != 4)
                {
                    Bricks[i] = _brickcharacter;
                    count++;
                }
                else
                {
                    Bricks[i] = _spacecharacter;
                    count = 0;
                }
            }
        }

        public bool Hit(int xposition)
        {
            if (Bricks[xposition-1] == _brickcharacter)
            {
                while (Bricks[xposition - 1] == _brickcharacter)
                    {
                        xposition++;
                        if (xposition == Bricks.Length+1)
                            break;
                    }
                

                xposition = xposition - 4;

                for (int i = 0; i < 4; i++)
                {
                    Bricks[xposition - 1] = _spacecharacter;

                    xposition++;
                }                
                return true;
            }
            else
            {
                return false;
            }
        }
	}

}
