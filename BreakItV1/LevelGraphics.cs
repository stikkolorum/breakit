using System;
using System.Collections.Generic;

namespace BreakItV1 {
	public class LevelGraphics {
		private readonly  String _header;
		private readonly  String _footer;
		private readonly int _width;
		private readonly int _height;
		private readonly  int _rows;

		private readonly char _leftuppercornercharacter = '\u250C';
		private readonly char _rightuppercornercharacter = '\u2510';
		private readonly char _leftlowercornercharacter = '\u2514';
		private readonly char _rightlowercornercharacter = '\u2518';
		private readonly char _vlinecharacter = '\u2502';
		private readonly char _hlinecharacter = '\u2500';
		private readonly char _ballcharacter = '⬤';
		private readonly char _paddlecharacter = '█';
		private readonly char _clearcharacter = ' ';
		private readonly String _paddlestring;
		private readonly Paddle _paddle;
		private readonly Ball _ball;
   
		public List<Row> Rows  { get; private set; }
        public String ScoreText { private get; set; }
		public String LivesText { private get; set; }

		public LevelGraphics(Paddle paddle, Ball ball, int width, int height, int rows)
        {
			_paddle = paddle;
			_ball = ball;
			_width = width;
			_height = height;
			_rows = rows;

			Rows = new List<Row>();

			_paddlestring = new string(_paddlecharacter, _paddle.Size);
			LivesText = _paddle.Lives.ToString();

			//build header
			_header += _leftuppercornercharacter;
			for (int i = 0; i < _width-2; i++)
			{
				_header += _hlinecharacter;
			}
			_header += _rightuppercornercharacter;

			//build rows
			for (int i = 0; i < _rows; i++)
            {
				Rows.Add(new Row(_width, new Random().Next(0,15)));
            }

			//build footer
			_footer += _leftlowercornercharacter;
			for (int i = 0; i < _width-2; i++)
			{
				_footer += _hlinecharacter;
			}
			_footer += _rightlowercornercharacter;

			DrawField();
		}

		public void DrawField()
        {
			Console.Clear();
			Console.WriteLine(_header);
		//	foreach (Row row in Rows)
		//	{
		//		Console.WriteLine(row.bricks);
		//	}
			for (int i = 0; i < _height - 2 ; i++)
			{
				Console.Write(_vlinecharacter);
				Console.Write(new String(' ', _width - 2));
				Console.WriteLine(_vlinecharacter);
			}
			Console.WriteLine(_footer);
		}

		public void Update() {
			Console.SetCursorPosition(0,0);

            for(int i = 0; i <Rows.Count; i++)
			{

				Console.SetCursorPosition(1, 1 + i);
                ConsoleColor current = Console.ForegroundColor;
				Console.ForegroundColor = (ConsoleColor)Rows[i].Color;
				Console.WriteLine(Rows[i].Bricks);
				Console.ForegroundColor = current;
			}

			//first clear old ball
			Console.SetCursorPosition(_ball.XPositionOld, _ball.YPositionOld);
			Console.Write(_clearcharacter);
            //draw new ball
            Console.SetCursorPosition(_ball.XPosition,_ball.YPosition);
			Console.Write(_ballcharacter);
            //clear old paddle
			Console.SetCursorPosition(_paddle.XPositionOld - (_paddlestring.Length / 2), _paddle.YPositionOld);
			Console.Write(new String(_clearcharacter,_paddlestring.Length));
            //draw new paddle
			Console.SetCursorPosition(_paddle.XPosition-(_paddlestring.Length/2), _paddle.YPosition);
			Console.Write(_paddlestring);
            //update score and lives text
			Console.SetCursorPosition(_width - 20, _height);
			Console.Write("Lives: "+_paddle.Lives.ToString());
			Console.SetCursorPosition(_width - 10, _height);
			Console.Write("Score: " + ScoreText);
		}
	}

}
