using System;
namespace BreakItV1 {
	public class Level {
        private int _width;
		private int _height;
		private int _rows;
        private Paddle _paddle;        
		private LevelGraphics _levelGraphics;
		private Ball _ball;
        //other place?
		private bool _update = true;

        public Direction paddledirection { get; set; }
		public int Score { get; private set; }

		public Level(int lives, int paddlesize, int width, int height, int rows)
        {
			_width = width;
			_height = height;
			_rows = rows;

            _ball = new Ball(_width / 2, _height-3, _height);
			_paddle = new Paddle(_width/2,_height-2,paddlesize, lives, _width);
			_levelGraphics = new LevelGraphics(_paddle, _ball, _width, _height, _rows);
        }

		public void CheckCollision() {
            //ball-field collission
			if (_ball.YPosition < 2 && _ball.vdirection == Direction.UP)
				_ball.vdirection = Direction.DOWN;

			if (_ball.YPosition > _height-3 && _ball.vdirection == Direction.DOWN)
            {
				_ball.Reset();
				_paddle.Die();
				_paddle.Reset();
			}

			if (_ball.XPosition < 2 && _ball.hdirection == Direction.LEFT)
				_ball.hdirection = Direction.RIGHT;

			if (_ball.XPosition > _width - 3 && _ball.hdirection == Direction.RIGHT)
				_ball.hdirection = Direction.LEFT;

			//ball-paddle collision
			int xdiff = Math.Abs(_ball.XPosition - _paddle.XPosition);

			if (_ball.YPosition == _paddle.YPosition-1 && xdiff < _paddle.Size-2 && _ball.vdirection == Direction.DOWN)
			{
				_ball.Angle = xdiff+1;
				_ball.vdirection = Direction.UP;
			}

            //ball-bricks collision
            for (int i = 0; i < _rows; i++)
            {
                if (_ball.YPosition < (3 + i) && _ball.vdirection == Direction.UP)
                {
					if (_levelGraphics.Rows[i].Hit(_ball.XPosition))
					{
						_ball.vdirection = Direction.DOWN;
						Score += _levelGraphics.Rows[i].Brickvalue;
						_levelGraphics.ScoreText = Score.ToString();
					}
                }
            }
        }

		public int CheckLevelStatus()
		{
			if (_paddle.Lives == -1)
            {
                return 1;
            }
			if (_rows * (_width - 1)/5 * _levelGraphics.Rows[0].Brickvalue == Score)
			{
				return 2;
			}

			else
				return 0;
		}

		public int Update() {
			if (_update)
			{
				_ball.Move();
			}

			_paddle.HDirection = paddledirection;
			_paddle.Move();          			

			if (_update)
				_update = false;
			else
				_update = true;

			CheckCollision();
			_levelGraphics.Update();

			//returns 0=playing,1=lost,2=won
			return CheckLevelStatus();
		}
	}

}
