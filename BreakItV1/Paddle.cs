using System;
namespace BreakItV1 {
	public class Paddle {

		private int _xinit;
		private int _yinit;
		private int _width;

		public int Lives { get; set;}
        public int Size { get; }

		public Direction HDirection { get; set; }
		public int XPosition { get; set; }
		public int YPosition { get; set; }
		public int XPositionOld { get; private set; }
		public int YPositionOld { get; private set; }

		public Paddle(int xposition, int yposition, int size, int lives, int width)
        {
			XPosition = _xinit = xposition;
			YPosition = _yinit = yposition;
			Size = size;
			Lives = lives;
			_width = width;
        }

        public void Move() {
			XPositionOld = XPosition;
			YPositionOld = YPosition;

			if (HDirection == Direction.RIGHT && XPosition < _width - Size/2 - 2)
				XPosition++;

			if (HDirection == Direction.LEFT && XPosition > 1 + Size / 2)
				XPosition--;
		}

		public void Clear() {
			throw new System.Exception("Not implemented");
		}
		public void Draw() {
			throw new System.Exception("Not implemented");
		}
		public void Die() {
			Lives--;
		}
		public void Reset() {
			XPosition = _xinit;
			YPosition = _yinit;
			HDirection = Direction.RIGHT;
		}


	}

}
